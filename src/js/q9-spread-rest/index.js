/* eslint-disable no-unused-vars,no-undef */
const assert = require('assert');

/**
 * # Question 9 - Spread/Rest
 */

const arr1 = [1, 2, 3, 4];
const arr2 = [5, 6, 7, 8];

// 9a - Using the spread operator create a new array `merged` with
// the contents of `arr1` and `arr2`

// START 9a answer code


const merged=[...arr1,...arr2]

console.log(...merged)

// END 9a answer code

assert.deepEqual(merged, [1, 2, 3, 4, 5, 6, 7, 8]);

// ----------------------------------------------------------------------------

// 9b - Create a function `add()` that sums a variable number of arguments
// using the rest operator.

// START 9b answer code

function add(...args){
    return args.reduce((prv,crnt)=>{
        return prv +crnt
    });
}

// END 9b answer code

console.assert(add(5, 15, 5, 5) === 30);

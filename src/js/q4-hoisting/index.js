/* eslint-disable */

/**
 * # Question 4:
 *
 * What is the result of invoking the function `log`?
 * Why?
 */

log('Hello!');

const log = function (output) {
  console.log(output);
}

/*
  WRITE YOUR ANSWER HERE
  ----------------------
hoisting means that you can call a function before declaring it. The result is an error, because of the 
method of declaration. It should be function log (output) 

*/

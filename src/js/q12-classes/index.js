/**
 * # Question 12 - Classes
 *
 * Convert the following constructor function to a class
 */

var Person = class {
    constructor(first, last) {
    this.first = first;
    this.last = last;
  }
}


Person.prototype.getName = function () {
  return this.first + ' ' + this.last;
};

/**
 * # Question 2 - Closure Counter
 * Refer to the README.md
 */

function makeCounter() {
  // Write your code here
}

// Do not modify anything below this point
const counter1 = makeCounter();
const counter2 = makeCounter(10);

console.assert(counter1.count === 0);
console.assert(counter2.count === 10);

counter1.increment();
counter2.decrement();

console.assert(counter1.count === 1);
console.assert(counter2.count === 9);

counter1.count = 5;
counter2.count = 5;

console.assert(counter1.count === 1);
console.assert(counter2.count === 9);

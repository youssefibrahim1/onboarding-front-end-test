/**
 * # Question 6:
 *
 * 1. What is the length of the Array?
 * 2. Why?
 * 3. What would be a better way to remove an item from an Array?
 */

const arr = [1, 2, 3];
delete arr[1];

/*
  WRITE YOUR ANSWER HERE
  ----------------------
  ## Answer 1
  The length of the array is 2  [1,3]
  ## Answer 2
  arr[1] which is the array at index 1 which had the value of two was deleted.
  ## Answer 3
  arr.splice(1,1);
*/

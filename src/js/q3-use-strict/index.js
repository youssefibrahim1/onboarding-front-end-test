/* eslint-disable strict,no-undef */

/**
 * # Question 3:
 * What is the error here
 */

'use strict';

 company = 'Alfan';

/*
  WRITE YOUR ANSWER HERE
  ----------------------

  Adding 'use strict' displays an error when there are certain poor coding practices.
  In this case because company was not defined an error is displayed. 
  To cancel this error define company (by adding var or const for example).

*/

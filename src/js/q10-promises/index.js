/* eslint-disable */

/**
 * # Question 10 - Promises
 * Refer to README.md
 */


// Code your answer below

let delay= (ms)=>{
    setTimeout(function () {
        `I was logged ${ms} ms after the call`
    }, ms);
}

 delay(1000)
  .then(() => {
    console.log('I was logged 1000 ms after the call');
  });


/* eslint-disable no-unused-vars */

/**
 * # Question 5 - Static Methods
 *
 * Refer to the README.md
 */

/**
 * @param {String} first defaults to 'unknown'
 * @param {String} last defaults to ''
 * @constructor
 */
function Person(first, last) {
  this.first = first;
  this.last = last; 

  const newPerson=`${first} ${last}`;
  return newPerson;
}

// const anonymous = new Person('unknown','');

// const anonymouse = new Person('Jane','Doe');

// function fullName(fst,lst){
// const newPerson = new Person(`${fst} ${lst}`)
// console.log(newPerson)
// }

// Do not modify anything below this point

const anonymous = new Person();

console.assert(anonymous.first === 'unknown');
console.assert(anonymous.last === '');
console.assert(anonymous.name() === 'unknown');

const jane = new Person('Jane', 'Doe');

console.assert(jane.first === 'Jane');
console.assert(jane.last === 'Doe');
console.assert(jane.name() === 'Jane Doe');
